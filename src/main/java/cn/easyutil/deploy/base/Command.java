package cn.easyutil.deploy.base;

import java.util.List;

/**
 * 指令集
 */
public class Command {

    //指令名称
    private String commandName;

    private CommandType type;

    //自定义指令集
    private List<String> commands;

    //执行自定义指令前的指令
    private List<String> preCommands;

    //执行自定义指令后的指令
    private List<String> postCommands;

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public Command(){}

    public Command(CommandType type){
        this.type = type;
        this.commandName = type.commandType;
    }

    public List<String> getCommands() {
        return commands;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }

    public List<String> getPreCommands() {
        return preCommands;
    }

    public void setPreCommands(List<String> preCommands) {
        this.preCommands = preCommands;
    }

    public List<String> getPostCommands() {
        return postCommands;
    }

    public void setPostCommands(List<String> postCommands) {
        this.postCommands = postCommands;
    }

    public CommandType getType() {
        return type;
    }

    public void setType(CommandType type) {
        this.type = type;
    }

}
