package cn.easyutil.deploy.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum CommandType {

    //重启命令
    RESTART("RESTARTJ"){
        @Override
        public List<String> getCommand(Command c) {
            if(c.getCommands() == null){
                c.setCommands(Arrays.asList("sh ${restartJ}"));
            }
            return super.getCommand(c);
        }
    },
    //关闭进程命令
    STOP("STOPJ"){
        @Override
        public List<String> getCommand(Command c) {
            if(c.getCommands() == null){
                c.setCommands(Arrays.asList("sh ${stopJ}"));
            }
            return super.getCommand(c);
        }
    },
    //启动命令
    START("STARTJ"){
        @Override
        public List<String> getCommand(Command c) {
            if(c.getCommands() == null){
                c.setCommands(Arrays.asList("sh ${startJ}"));
            }
            return super.getCommand(c);
        }
    },
    //自定义命令
    CUSTOM("COSTOM");

    public String commandType;

    CommandType(String commandType){
        this.commandType = commandType;
    }

    public static CommandType getType(String commandType){
        for (CommandType value : CommandType.values()) {
            if(commandType.toUpperCase().contains(value.commandType)){
                return value;
            }
        }
        return CUSTOM;
    }

    public List<String> getCommand(Command c){
        List<String> cmds = new ArrayList<>();
        if(c.getPreCommands() != null){
            cmds.addAll(c.getPreCommands());
        }
        if(c.getCommands() != null){
            cmds.addAll(c.getCommands());
        }
        if(c.getPostCommands() != null){
            cmds.addAll(c.getPostCommands());
        }
        return cmds;
    }
}
