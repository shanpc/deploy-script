package cn.easyutil.deploy.base;

/**
 * 操作系统类型
 */
public enum SystemType {

    WINDOWS("GBK","windows"),
    LINUX("utf-8","linux");

    private String charset;

    private String name;

    private SystemType(String charset,String...name){
        this.charset = charset;
        if(name.length > 0){
            this.name = name[0];
        }
    }

    public static SystemType current(){
        //获取当前操作系统
        String osName = System.getProperty("os.name");
        if(osName.trim().toUpperCase().contains("WINDOWS")){
            return WINDOWS;
        }
        return LINUX;
    }


    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
