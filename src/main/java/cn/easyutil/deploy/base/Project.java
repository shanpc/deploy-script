package cn.easyutil.deploy.base;

import java.util.*;

public class Project {

    private String projectName;
    //java、
    private ProjectType type;
    //java项目对应的jar文件地址
    private String jarPath;
    //jvm参数
    private String jvmParam;
    //jar 启动参数
    private String jarParam;
    //快捷指令集
    private Map<String, Command> shortcut;
    //自定义指令集
    private Map<String,Command> commands;

    //自定义脚本(key:脚本名称，val:脚本执行命令)
    private Map<String,String> scripts;
    //指令执行类型  本地、远程
    private ExecType execType = ExecType.local;
    //远程机器ip
    private String ip;
    //远程机器用户名
    private String username;
    //远程机器密码
    private String password;

    public Command getShortcutCommand(String name){
        if(shortcut==null || shortcut.size()==0){
            return null;
        }
        return shortcut.get(name);
    }

    public Command getCommandsCommand(String name){
        if(commands==null || commands.size()==0){
            return null;
        }
        return commands.get(name);
    }

    public Command getCommand(String name){
        Command command = getShortcutCommand(name);
        if(command == null){
            command = getCommandsCommand(name);
        }
        return command;
    }

    public List<String> parseCommand(List<String> commands){
        if(this.getScripts()==null || this.getScripts().size()==0){
            return commands;
        }
        List<String> result = new ArrayList<>();
        Set<String> keys = this.getScripts().keySet();
        for (String cmd : commands) {
            while(true){
                if(!cmd.contains("${") || !cmd.contains("}")){
                    break;
                }
                String script = cmd.substring(cmd.indexOf("${")+2,cmd.indexOf("}"));
                if(keys.contains(script)){
                    cmd = cmd.replace("${"+script+"}",this.getScripts().get(script));
                }
            }
            result.add(cmd);
        }
        return result;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public ProjectType getType() {
        return type;
    }

    public void setType(ProjectType type) {
        this.type = type;
    }

    public String getJarPath() {
        return jarPath;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public Map<String, Command> getShortcut() {
        return shortcut;
    }

    public void setShortcut(Map<String, Command> shortcut) {
        this.shortcut = shortcut;
    }

    public Map<String, Command> getCommands() {
        if(this.commands == null){
            return new HashMap<>();
        }
        return commands;
    }

    public void setCommands(Map<String, Command> commands) {
        this.commands = commands;
    }

    public String getJvmParam() {
        return jvmParam;
    }

    public void setJvmParam(String jvmParam) {
        this.jvmParam = jvmParam;
    }

    public Map<String, String> getScripts() {
        if(this.scripts == null){
            return new HashMap<>();
        }
        return scripts;
    }

    public void setScripts(Map<String, String> scripts) {
        this.scripts = scripts;
    }

    public ExecType getExecType() {
        return execType;
    }

    public void setExecType(ExecType execType) {
        this.execType = execType;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getJarParam() {
        return jarParam;
    }

    public void setJarParam(String jarParam) {
        this.jarParam = jarParam;
    }
}
