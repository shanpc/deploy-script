package cn.easyutil.deploy.executor;

import cn.easyutil.deploy.base.Project;

public class CommandExecutorBuilder {


    public static AbstractExecutor build(Project project){
        return build(project,null,null,null,null,null);
    }

    public static AbstractExecutor build(Project project,CommandResultCallBack callBack){
        return build(project,null,null,null,null,callBack);
    }

    /**
     * 获取执行器
     * @param project   项目对象
     * @param interruptConsoleMaxTime   中断执行的最大等待时间
     * @param interruptConsoleText  中断执行的文字
     * @param consoleCharSet    输出字符集
     * @param isWaitFor     是否等待控制台打印信息返回
     * @param callBack  处理控制台打印信息
     * @return
     */
    public static AbstractExecutor build(Project project,
                                         Long interruptConsoleMaxTime,
                                         String interruptConsoleText,
                                         String consoleCharSet,
                                         Boolean isWaitFor,
                                         CommandResultCallBack callBack){
        AbstractExecutor executor = null;
        switch (project.getExecType()){
            case local:
                executor =  new LocalCommandExecutor();
                break;
            case remote:
                executor =  new RemoteCommandExecutor(project.getIp(),project.getUsername(),project.getPassword());
                break;
        }
        executor.setCallBack(callBack);
        executor.setConsoleCharSet(consoleCharSet);
        executor.setInterruptConsoleMaxTime(interruptConsoleMaxTime);
        executor.setInterruptConsoleText(interruptConsoleText);
        executor.setWaitFor(isWaitFor);
        return executor;
    }
}
