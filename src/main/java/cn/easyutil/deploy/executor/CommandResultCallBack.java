package cn.easyutil.deploy.executor;

/**
 * 执行结果回调
 */
public interface CommandResultCallBack {

    void callBack(String console);
}
