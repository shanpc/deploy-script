package cn.easyutil.deploy.util;

import cn.easyutil.deploy.env.ShortcutBuilder;

import java.util.List;
import java.util.Scanner;

public class SystemIn {
    public static int check(List<String> projects){
        if(projects.size() == 1){
            return 0;
        }
        System.out.println("--------please check id--------");
        for (int i = 0; i < projects.size(); i++) {
            System.out.println(i+"."+projects.get(i));
        }
        while (true){
            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            try {
                int id = Integer.valueOf(line);
                String s = projects.get(id);
                if(s != null){
                    return id;
                }
            }catch (Exception e){
                System.out.println("--------please check right id--------");
            }
        }
    }

    public static void help(){
        System.out.println("--------配置文件yaml内容样例--------");
        System.out.println(ShortcutBuilder.build().example());
        System.out.println("--------任意按键退出--------");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
