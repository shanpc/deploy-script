package cn.easyutil.deploy.env;

import cn.easyutil.deploy.base.SystemType;

public class ShortcutBuilder {

    public static JavaShortcut build(){
        if(SystemType.current() == SystemType.LINUX){
            return new ShellShotcut();
        }
        return new ShellShotcut();
    }
}
