package cn.easyutil.deploy.env;

import cn.easyutil.deploy.base.Command;
import cn.easyutil.deploy.base.Project;
import cn.easyutil.deploy.base.CommandType;
import cn.easyutil.deploy.base.ExecType;
import cn.easyutil.deploy.base.ProjectType;
import cn.easyutil.deploy.util.RemoteUtil;

import java.io.File;
import java.util.*;

/**
 * 配置文件
 */
public class SettingConfig {

    //默认读取相对路径下的文件
    private static final File configFile = new File(System.getProperty("user.dir")+File.separator+"deployScript.yaml");
    {
        System.out.println("defaultConfigPath:"+configFile.getAbsolutePath());
    }
    //存放所有的配置文件
    private Map<String,Object> config = new HashMap<String, Object>();

    //解析后的配置文件
    private Map<String, Project> source = new HashMap<>();

    //默认远程机器上的脚本文件路径
    public static String defaultRemoteScriptFilePath = "/easy-deploy-script/";

    public SettingConfig(File file){
        if(file == null){
            this.config = ConfigFileLoad.loadYml(configFile);
            this.parseConfig();
            return ;
        }
        if(file!=null && !file.exists()){
            throw new RuntimeException("file not found with:"+file.getAbsolutePath());
        }
        String absolutePath = file.getAbsolutePath();
        if(absolutePath.substring(absolutePath.lastIndexOf(".")).toUpperCase().equals("YML")){
            this.config = ConfigFileLoad.loadYml(file);
        }
        if(absolutePath.substring(absolutePath.lastIndexOf(".")).toUpperCase().equals("XML")){
            this.config = ConfigFileLoad.loadXml(file);
        }
        this.parseConfig();
    }

    /**
     * 实例化配置文件
     * @param source
     * @return
     */
    public SettingConfig(Map<String, Project> source){
        this.source = source;
    }

    private void parseConfig(){
        if(this.config == null){
            return ;
        }
        Iterator<Map.Entry<String, Object>> iterator = this.config.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey();
            Object value = next.getValue();
            if(value==null || !(value instanceof Map)){
                continue;
            }
            Project project = parseProject((Map) value,key);
            if(project == null){
                continue;
            }
            project.setProjectName(key);
            //初始化java项目的命令
            initJavaCommand(project);
            //如果项目为远程，则将脚本文件拷贝到远程服务器
            initRemoteScript(project);
            //组装项目
            source.put(key,project);
        }
    }

    /**
     * 将脚本拷贝到远程项目
     * @param project
     */
    private void initRemoteScript(Project project) {
        if(project.getExecType() != ExecType.remote){
            return ;
        }
        if(project.getScripts()==null || project.getScripts().size()==0){
            return ;
        }
        RemoteUtil util = new RemoteUtil(project.getIp(),project.getUsername(),project.getPassword());
        util.exec("mkdir "+defaultRemoteScriptFilePath);

        Map<String,String> script = new HashMap<>();
        Iterator<Map.Entry<String, String>> iterator = project.getScripts().entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> next = iterator.next();
            String key = next.getKey();
            String value = next.getValue();
            File file = new File(value);
            util.scpPut(file,defaultRemoteScriptFilePath);
            script.put(key,defaultRemoteScriptFilePath+File.separator+file.getName());
        }
        project.setScripts(script);
    }

    private void initJavaCommand(Project project){
        if(project == null){
            return ;
        }
        //处理java项目的默认集成命令_java_start,_java_restart,_java_stop
        if(project.getType() != ProjectType.JAVA){
            return ;
        }
        //先检查用户有没有重新自定义java脚本,没有的话自动生成脚本
        JavaShortcut build = ShortcutBuilder.build();
        if(project.getExecType() == ExecType.remote){
            build.setRemotePath(defaultRemoteScriptFilePath);
        }

        //------------
        Map<String, String> scripts = project.getScripts();
        Map<String, Command> commands = project.getCommands();
        if(commands == null){
            commands = new HashMap<>();
        }
        Command startJ = null;
        Command stopJ = null;
        Command restartJ = null;
        Set<String> keys = commands.keySet();
        for (String key : keys) {
            if(key.toUpperCase().equals("STARTJ")){
                startJ = commands.get(key);
            }else if(key.toUpperCase().equals("RESTARTJ")){
                restartJ = commands.get(key);
            }else if(key.toUpperCase().equals("STOPJ")){
                stopJ = commands.get(key);
            }
        }
        if(startJ == null){
            startJ = new Command();
            startJ.setPreCommands(Arrays.asList());
            startJ.setPostCommands(Arrays.asList());
            commands.put("startJ",new Command(CommandType.START));
        }
        if(startJ.getCommands() == null){
            String s = build.start(project.getProjectName(), project.getJarPath(), project.getJvmParam(), project.getJarParam());
            startJ.setCommands(Arrays.asList(s));
        }
        String startScript = cmdToScript(startJ.getPreCommands())+cmdToScript(startJ.getCommands())+cmdToScript(startJ.getPostCommands());
        scripts.put("startJ",build.script("."+project.getProjectName()+"_startJ",startScript));

        if(stopJ == null){
            stopJ = new Command();
            stopJ.setPreCommands(Arrays.asList());
            stopJ.setPostCommands(Arrays.asList());
            commands.put("stopJ",new Command(CommandType.STOP));
        }
        if(stopJ.getCommands() == null){
            String s = build.stop(project.getProjectName(),project.getJarPath());
            stopJ.setCommands(Arrays.asList(s));
        }
        String stopScript = cmdToScript(stopJ.getPreCommands())+cmdToScript(stopJ.getCommands())+cmdToScript(stopJ.getPostCommands());
        scripts.put("stopJ",build.script("."+project.getProjectName()+"_stopJ",stopScript));

        if(restartJ == null){
            restartJ = new Command();
            restartJ.setPreCommands(Arrays.asList());
            restartJ.setPostCommands(Arrays.asList());
            commands.put("restartJ",new Command(CommandType.RESTART));
        }
        if(restartJ.getCommands() == null){
            String s = build.restart(project.getProjectName());
            restartJ.setCommands(Arrays.asList(s));
        }
        String restartScript = cmdToScript(restartJ.getPreCommands())+cmdToScript(restartJ.getCommands())+cmdToScript(restartJ.getPostCommands());
        scripts.put("restartJ",build.script("."+project.getProjectName()+"_restartJ",restartScript));
        project.setCommands(commands);
        project.setScripts(scripts);
        System.out.println();
        //------------
//        if(scripts.get("startJ") == null){
//            //构建java脚本
//            String startJ = build.start(project.getProjectName(),project.getJarPath(),project.getJvmParam(),project.getJarParam());
//            scripts.put("startJ",startJ);
//        }
//        if(scripts.get("restartJ") == null){
//            //构建java脚本
//            String restartJ = build.restart(project.getProjectName());
//            scripts.put("restartJ",restartJ);
//        }
//        if(scripts.get("stopJ") == null){
//            //构建java脚本
//            String stopJ = build.stop(project.getProjectName(),project.getJarPath());
//            scripts.put("stopJ",stopJ);
//        }
//        project.setScripts(scripts);
//
//        //再检查用户有没有重新定义java指令，没有的话自动添加
//        Map<String, Command> commands = project.getCommands();
//        boolean hasStartJ = false;
//        boolean hasResStartJ = false;
//        boolean hasStopJ = false;
//        for (String key : commands.keySet()) {
//            key = key.toUpperCase();
//            if(key.equals("STARTJ")){
//                hasStartJ = true;
//            }
//            if(key.equals("RESTARTJ")){
//                hasResStartJ = true;
//            }
//            if(key.equals("STOPJ")){
//                hasStopJ = true;
//            }
//        }
//        if(!hasStartJ){
//            //构建java指令
//            commands.put("startJ",new Command(CommandType.START));
//        }
//        if(!hasResStartJ){
//            //构建java指令
//            commands.put("restartJ",new Command(CommandType.RESTART));
//        }
//        if(!hasStopJ){
//            //构建java指令
//            commands.put("stopJ",new Command(CommandType.STOP));
//        }
//        project.setCommands(commands);
    }

    /**
     * 包装项目
     * @param map
     * @return
     */
    private Project parseProject(Map<String,Object> map,String projectName){
        if(map == null){
            return null;
        }
        //组装project
        Project project = new Project();
        project.setType(ProjectType.JAVA);
        Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        //获取项目下的配置信息
        while (iterator.hasNext()){
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey().toUpperCase();
            Object value = next.getValue();
            if(key.equals("TYPE") && value.toString().toUpperCase().equals("JAVA")){
                //项目类型
                project.setType(ProjectType.JAVA);
            }
            if(key.equals("JARPATH")){
                //获取java的jar路径
                project.setJarPath(value.toString());
            }
            if(key.equals("JVMPARAM")){
                //获取jvm启动参数
                project.setJvmParam(value.toString());
            }
            if(key.equals("JARPARAM")){
                //获取jar启动参数
                project.setJarParam(value.toString());
            }
            if(key.equals("SCRIPT")){
                //脚本封装
                project.setScripts(parseScript(value,projectName));
            }
            if(key.equals("COMMAND") && (value instanceof Map)){
                //指令封装
                project.setCommands(parseProjectCommand((Map) value));
            }
            if(key.equals("EXECTYPE")){
                project.setExecType(ExecType.remote);
                if(value.toString().toUpperCase().equals("LOCAL")){
                    project.setExecType(ExecType.local);
                }
            }
            if(key.equals("IP")){
                project.setIp(value.toString());
            }
            if(key.equals("USERNAME")){
                project.setUsername(value.toString());
            }
            if(key.equals("PASSWORD")){
                project.setPassword(value.toString());
            }
        }
        return project;

    }

    /**
     * 组装脚本
     * @param script
     * @param projectName
     * @return
     */
    private Map<String,String> parseScript(Object script,String projectName){
        if(script==null || (!(script instanceof Map))){
            return new HashMap<>();
        }
        JavaShortcut build = ShortcutBuilder.build();
        Map<String,String> scMap = new HashMap<>();
        //处理和生成脚本文件
        Map<String,Object> sc = (Map) script;
        Iterator<Map.Entry<String, Object>> it = sc.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry<String, Object> n = it.next();
            String k = n.getKey();
            String v = n.getValue().toString();
            scMap.put(k,build.script("."+projectName+"_"+k,v));
        }
        return scMap;
    }

    private Map<String,Command> parseProjectCommand(Map<String,Object> map){
        if(map == null){
            return new HashMap<>();
        }
        Map<String,Command> result = new HashMap<>();
        Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Object> next = iterator.next();
            String key = next.getKey();
            Object value = next.getValue();
            if(value==null || !(value instanceof Map)){
                continue;
            }
            //指令对象
            Command command = new Command();
            command.setCommandName(key);
            command.setType(CommandType.getType(key));
            //解析自定义指令集
            Object cmd = ((Map) value).get("cmd");
            if(cmd!=null && (cmd instanceof Map)){
                command.setCommands(new ArrayList<>(((Map) cmd).values()));
            }
            //解析前置指令
            Object pre = ((Map) value).get("pre");
            if(pre!=null && (pre instanceof Map)){
                command.setPreCommands(new ArrayList<>(((Map) pre).values()));
            }
            //解析后置指令
            Object post = ((Map) value).get("post");
            if(post!=null && (post instanceof Map)){
                command.setPostCommands(new ArrayList<>(((Map) post).values()));
            }
            //组装最终指令
            result.put(key,command);
        }
        return result;
    }

    /**
     * 获取全部项目
     * @return
     */
    public List<String> projects(String name){
        Set<String> set = source.keySet();
        if(name == null){
            return new ArrayList<>(set);
        }
        List<String> result = new ArrayList<>();
        for (String key : set) {
            if(key.toUpperCase().contains(name.toUpperCase())){
                result.add(key);
            }
        }
        return result;
    }

    public Project getProject(String name){
        return source.get(name);
    }

    public List<String> getCommands(Project project,String name){
        Set<String> all = new HashSet<>();
        Map<String, Command> commands = project.getCommands();
        if(commands != null){
            all.addAll(commands.keySet());
        }
        Map<String, Command> shortcut = project.getShortcut();
        if(shortcut != null){
            all.addAll(shortcut.keySet());
        }
        if(name == null){
            return new ArrayList<>(all);
        }
        List<String> result = new ArrayList<>();
        for (String key : all) {
            if(key.toUpperCase().contains(name.toUpperCase())){
                result.add(key);
            }
        }
        return result;
    }


    /**
     * 将多条指令合并成一个脚本字符串
     * @param cmds
     * @return
     */
    private String cmdToScript(List<String> cmds){
        if(cmds==null || cmds.isEmpty()){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (String cmd : cmds) {
            sb.append(System.getProperty("line.separator"));
            sb.append(cmd);
        }
        return sb.toString();
    }
}
