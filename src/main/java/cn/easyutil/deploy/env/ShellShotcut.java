package cn.easyutil.deploy.env;

import java.io.File;

/**
 * shell快捷脚本
 */
public class ShellShotcut extends JavaShortcut{

    @Override
    public String restart(String projectName) {

        //组装脚本
        StringBuffer sb = new StringBuffer();
        sb.append("cd "+remotePath);
        sb.append(line());
        sb.append("sh ."+projectName+"_stopJ.sh");
        sb.append(line());
        sb.append("sleep 5s");
        sb.append(line());
        sb.append("sh ."+projectName+"_startJ.sh");

        //创建一个隐藏文件
        return sb.toString();
    }

    @Override
    public String start(String projectName,String jarPath,String jvmParam,String jarParam) {
        StringBuffer sb = new StringBuffer();
        sb.append("nohup java ");
        if(jvmParam!=null && jvmParam.length()>0){
            sb.append(jvmParam);
        }
        sb.append(" -jar ");
        sb.append(jarPath);
        sb.append(" ");
        if(jarParam!=null && jarParam.length()>0){
            sb.append(jarParam);
        }
        sb.append(" > "+remotePath+projectName+"-nohup.out 2>&1 &");
        sb.append(line());
        sb.append("tail -f "+remotePath+projectName+"-nohup.out");
        return sb.toString();
    }

    @Override
    public String stop(String projectName,String jarPath) {
        //获取jar包名字
        String jarName = jarPath.substring(jarPath.lastIndexOf(File.separator) + 1);

        StringBuffer sb = new StringBuffer();
        sb.append("if [ $(ps -ef |grep ${jarName}|grep -v grep|wc -l) -ge 1 ];");
        sb.append(line());
        sb.append(" then kill -9  $(pgrep -f ${jarName})");
        sb.append(line());
        sb.append("fi");

        //替换jar真实名称
        String script = sb.toString().replace("${jarName}",jarName);
        //创建一个隐藏文件
        return script;
    }

    @Override
    public String script(String fileName, String text) {
        return super.script(fileName+".sh", text);
    }
}
