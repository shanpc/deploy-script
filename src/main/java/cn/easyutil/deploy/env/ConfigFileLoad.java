package cn.easyutil.deploy.env;

import cn.easyutil.deploy.util.FileUtil;
import cn.easyutil.deploy.util.XmlUtil;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 加载配置文件
 */
public class ConfigFileLoad {

    public static Map<String,Object> loadYml(File file){
        Yaml yaml = new Yaml();
        //通过yaml对象将配置文件的输入流转换成map原始map对象
        return yaml.loadAs(FileUtil.toInput(file.getAbsolutePath()), Map.class);
    }

    public static Map<String,Object> loadXml(File file){
        return XmlUtil.xmlToMap(FileUtil.read(file, StandardCharsets.UTF_8));
    }

    public static Map<String,Object> loadPropertites(File file){

        return null;
    }
}
