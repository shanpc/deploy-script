package cn.easyutil.deploy.env;

import java.io.File;
import java.io.FileOutputStream;

/**
 * 快捷脚本
 */
public abstract class JavaShortcut {
    //当前项目路径
    protected String currentPath = System.getProperty("user.dir")+File.separator;
    //远程文件路径
    protected String remotePath = currentPath;

    public String restart(String projectName){
        throw new RuntimeException("no cmd file");
    };

    public String start(String projectName,String jarPath,String jvmParam,String jarParam){
        throw new RuntimeException("no cmd file");
    };

    public String stop(String projectName,String jarPath){
        throw new RuntimeException("no cmd file");
    };

    public String script(String fileName,String text){
        if(fileName==null || text==null){
            throw new RuntimeException("not find cmd");
        }
        File file = new File(currentPath + fileName);
        saveFile(file,text);
        return file.getAbsolutePath();
    };

    public String example(){
        return "#这里填写项目名称\n" +
                "projectName:\n" +
                "  #本地命令(local)、远程命令(remote,默认本地,远程暂时未实现脚本文件命令执行)\n" +
                "  execType: remote\n" +
                "  #远程ip(本地无需配置)\n" +
                "  ip: 192.168.200.201\n" +
                "  #远程登陆用户名(本地无需配置)\n" +
                "  username: root\n" +
                "  #远程登陆密码(本地无需配置)\n" +
                "  password: Wsx123!@#\n" +
                "  #项目类型，(非java项目无需配置)\n" +
                "  type: java\n" +
                "  #如果是java项目，此处填写jar包所在位置(非java项目无需配置)\n" +
                "  jarPath: /workspace/project/xxxx/java/target/xxx.jar\n" +
                "  #如果是java项目，此处填写jvm参数(非java项目无需配置)\n" +
                "  jvmParam: \"-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=8091 -Djava.security.egd=file:/dev/./urandom\"\n" +
                "  #自定义命令\n" +
                "  command:\n" +
                "    #快捷启动命令(非必须)\n" +
                "    startj:\n" +
                "      #自定义启动命令(非必须)\n" +
                "      cmd:\n" +
                "        #执行的第一条命令\n" +
                "        1: \"cd /\"\n" +
                "        #执行的第二条命令\n" +
                "        2: \"mvn clean\"\n" +
                "        #执行的第三条命令\n" +
                "        3: \"xxx\"\n" +
                "      #执行启动命令前需要执行的命令(非必须)\n" +
                "      pre:\n" +
                "        1: \"xxx\"\n" +
                "        2: \"xxx\"\n" +
                "      #执行启动命令后需要执行的命令(非必须)\n" +
                "      post:\n" +
                "        1: \"xxx\"\n" +
                "        2: \"xxx\"\n" +
                "        3: sh ${test2}\n" +
                "    #快捷结束进程命令(非必须)\n" +
                "    stopJ:\n" +
                "    #快捷重启进程命令(非必须)\n" +
                "    restartJ:\n" +
                "    #自定义命令的名称\n" +
                "    install-test:\n" +
                "      #执行命令\n" +
                "      cmd:\n" +
                "        1: \"xxx\"\n" +
                "        2: \"xxx\"\n" +
                "        #引用脚本执行\n" +
                "        3: sh ${test1}\n" +
                "        \n" +
                "  #脚本文件内容\n" +
                "  script:\n" +
                "    #脚本名称和内容\n" +
                "    test1: \"if [ $(ps -ef |grep cq_house.jar|grep -v grep|wc -l) -ge 1 ]; then \\n\n" +
                "              kill -9  $(pgrep -f cq_house.jar)\\n\n" +
                "            fi\\n\n" +
                "           \"\n" +
                "    test2: \"hahaha\\n\n" +
                "              1111111111\\n\n" +
                "            hahaha\\n\n" +
                "           \"\n" +
                "\n" +
                "\n" +
                "#第二个项目配置(极简)\n" +
                "projectName1:\n" +
                "  command:\n" +
                "    install:\n" +
                "      cmd:\n" +
                "        0: sh ${test1}\n" +
                "#启动命令 -D跟项目名(支持模糊) --跟命令(支持模糊)\n" +
                "#java -jar ins.jar -DprojectName --install";

    }

    void saveFile(File file,String text){
        try(FileOutputStream out = new FileOutputStream(file);){
            out.write(text.getBytes());
            out.flush();
        }catch (Exception e){}
    }

    private String tabWithLine(int line){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < line; i++) {
            sb.append(tab());
        }
        return sb.toString();
    }

    /**
     * 换行
     * @return
     */
    String line(){
        return System.getProperty("line.separator");
    }

    /**
     * 空格
     * @return
     */
    String space(){
        return " ";
    }

    /**
     * 制表符
     * @return
     */
    String tab(){
        return "\t";
    }

    public void setRemotePath(String remotePath) {
        this.remotePath = remotePath;
    }
}
