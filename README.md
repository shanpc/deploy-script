# deployScript

#### 介绍
一个java编写的脚本执行程序，可以通过配置文件自动执行配置的脚本，非常适合多项目管理，内置提供了对java项目的快捷指令，start、stop、restart

#### 启动教程

1.  maven 打包
2.  配置 deployScript.yaml文件
3.  执行 java -jar ins.jar

#### 启动命令

1.  java -jar ins.jar -DprojectName --restart
2.  java -jar ins.jar --help
####
说明：
-D：跟项目名称（支持模糊查找）
--: 跟指令名称（支持模糊查找，可以是便捷指令，也可以是自定义指令）


#### 样例
![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/150000_0554c4f9_1255698.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/150131_cc688823_1255698.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/150308_1b0e7696_1255698.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0108/150407_70a762a3_1255698.png "屏幕截图.png")